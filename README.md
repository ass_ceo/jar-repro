# README

This repository is a reproduction of my personal repo.

The following are the steps to get up and running:

<h1>1. Install dependencies</h1>

Run `bundle install` to install necessary gems.

<h1>2. Create private and public keys</h1>

First, navigate the `/tmp` folder and create a `rsaprivkey.pem` and `rsapubkey.pem` file. Once you've done this, you can use the openssl cli tool or navigate to the [this link](https://cryptotools.net/rsagen) to generate keys.

<h1>3. Create the database</h1>

Start your postgres server and run the following commands:

```
rails db:create
rails db:migrate
```

<h1>4. Precompile assets</h1>

Run the following command to precompile assets.

```
rails assets:precompile
```

<h1>5. Run the server</h1>
Run the following command to start the rails server:

```
./bin/dev
```

<h1>6. Create Account and Register Client</h1>

Go to `/create-account` and make an account.

After you make your account, paste the following into the terminal:

```
curl --location -v --request POST 'http://localhost:3000/register?client_name=App&description=app&client_uri=http%3A%2F%2Flocalhost%3A3000&redirect_uris[]=http%3A%2F%2Flocalhost%3A3000%2Fcallback&scopes=openid%20email&request_object_signing_alg=RS256&jwks_uri=http://localhost:3000/jwks'
```

<h1>7. Create the JWT</h1>
First `GET http://localhost:3000/jwks` and copy only the jwks object. Go to [jwt.io](jwt.io) and paste the object into the public key area. Be sure to replace `kid`, `client_id`, `iat`, `nbf`, `exp`, and `jti` with the appropriate params. You can use [this link](https://www.unixtimestamp.com) to get the CurrentTime unix timestamp and use [this link](https://www.freeformatter.com/message-digest.html) to generate the SHA256HEXDIGEST. Input the following into the form.

<h2>Header</h2>
{
  "alg": "RS256",
  "typ": "application/oauth-authz-req+jwt",
  "kid": "{kid}"
}
<h2>Payload</h2>
{
"iss": "{client_id}",
"aud": "http://localhost:3000", "client_id": "{client_id}",
"response_type": "code", 
"redirect_uri": "http://localhost:3000/callback", 
"scope": "openid email", 
"code_challenge": "M2CSwNxM1y-2ZWV9E0h9wznjlpqJq5y2kdpHhAwMisA", "code_challenge_method": "S256",
"state": "oo88zagr5xcrtmm",
"nonce": "gaipsnw66xpbxd2", 
"exp": {CurrentTime + 1800}, 
"iat": {CurrentTime},  
"nbf": {CurrentTime},
"jti": "{SHA256HEXDIGEST(http://localhost:3000:{CurrentTime})}"
}
<h2>Verify Signature</h2>
Input jwk object as public key and input the private key into the private key form.

<h1>8. Create the par request</h1>

To reproduce the issues with `/par`, copy the curl request below. Be sure to replace the `Base64EncodedClientId:Secret` placeholder in the Authorization header along with the `client_id` and `request` placeholders in the body. The request placeholder should contain the jwt we generated in the last step

```
curl --location 'http://localhost:3000/par' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Authorization: Basic {Base64EncodedClientId:Secret}' \
--data-urlencode 'client_id={client_id}' \
--data-urlencode 'request={jwt}
```

<h1>8. Route to Authorize Endpoint</h1>

Take the `request_uri` in the response from the curl request and paste the link below into your browser. Again, be sure to replace the placeholders in the link with your appropriate `client_id` and `request_uri`.

```
http://localhost:3000/authorize?client_id={client_id}&request_uri={request_uri}
```

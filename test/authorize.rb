# frozen_string_literal: true

require "test_helper"

PKCE_CHALLENGE="7051b9207dd966c4cb1f1b5334b6be9b5e85f7ea2896b509be7c522d128ea8fe195a28b5b67f8d15609635241075c2bfac5d6e7bee61122833db94f8dc903b63"

class RodauthOauthPkceAuthorizeTest < ActionDispatch::IntegrationTest

  test "create and verify account" do
    click_on "Sign up"
    fill_in "Name", with: "Janko"
    fill_in "Email", with: "janko@hey.com"
    click_on "Create Account"

    assert_match "An email has been sent to you with a link to verify your account", page.text
    assert_equal "/", page.current_path
    assert_match "Janko", page.text

    visit email_link
    fill_in "Password",         with: "secret"
    fill_in "Confirm Password", with: "secret"
    click_on "Verify Account"

    assert_match "Your account has been verified", page.text
    assert_equal "/", page.current_path
    assert_match "Janko", page.text
  end

  test "register client" do
    get register_url

    assert_redirected_to "/login"

    login

    get register_url

    assert_response :success
  end

  test "authorize client" do
    get authorize_url

    assert_redirected_to "/login"

    login

    get authorize_url

    assert_response :success
  end



  private
  def create_account(email: "janko@hey.com", password: "secret")
    click_on "Sign up"
    fill_in "Name", with: "Janko"
    fill_in "Email", with: email
    click_on "Create Account"

    visit RodauthMailer.deliveries.last.body.to_s[%r{https?://\S+}]
    fill_in "Password",         with: password
    fill_in "Confirm Password", with: password
    click_on "Verify Account"
  end

  def register
    post "/register", params: {
  "client_name" =>"Client Posts App",
  "description"=> "A app showing my posts from this one",
   "client_uri"=> "http://localhost:9293",
  "redirect_uris"  => "[http://localhost:9293/auth/openid_connect/callback]",
  "scopes"=>"openid email",
    }
  end

  def authorize
    post "/authorize", params: {
      "client_name" =>"Client Posts App",
      "description"=> "A app showing my posts from this one",
      "client_uri"=> "http://localhost:9293",
      "redirect_uris"  => "[http://localhost:9293/auth/openid_connect/callback]",
      "scopes"=>"openid email",
    }
  end



end

class ApplicationController < ActionController::Base

  # private

  def require_authentication
    rodauth.require_authentication
  end

end

require 'securerandom'
require 'base64'
require 'openssl'

class RodauthApp < Rodauth::Rails::App
  # primary configuration
  configure RodauthOauth

  route do |r|
    r.rodauth

    rodauth.load_openid_configuration_route
    rodauth.load_oauth_server_metadata_route
    rodauth.require_authentication

    r.on "oidc-logout" do
      rodauth.check_csrf false
    end

    r.on "authorize" do
      unless r.params["content-type"]
        r.params["content-type"] = "application/oauth-authz-req+jwt"
      end
    end

    r.on "callback" do
      # r.post do
      if request.params["error"]
        flash[:error] = "Authorization failed: #{r.params['error_description'] || r.params['error']}"
      end
      session_state = session.delete("state")

      if session_state
        state = request.params["state"]
        if !state || state != session_state
          flash[:error] = "state doesn't match, CSRF Attack!!!"
          r.redirect "/"
        end
      end

      code = r.params["response"]
      # red_url = "com.amirs.app://expo-development-client/?url=http%3A%2F%2F10.14.9.167%3A8081/(screens)/(auth)/authenticate?response=#{response}"
      red_url = "com.amirs.app://expo-development-client/?url=http%3A%2F%2F10.14.9.167%3A8081/(screens)/(auth)/authenticate?response=#{code}"

      r.redirect red_url
      # end
    end

    r.root do
      view inline: <<~HTML
        <% if rodauth.logged_in? %>
        <h1 class="lead">
          Successful login.
        </h1>
        <% else %>
          <p class="lead">
            This is the demo authentication server for <a href="https://gitlab.com/os85/rodauth-oauth">Roda Oauth - Open ID Connect</a>.
            Roda Oauth extends Rodauth to support the OAuth 2.0 authorization protocol, while adhering to the same principles of the parent library.
          </p>
          <p class="lead">In the authentication server, you can setup your account, and also register client applications.</p>
          <p class="text-center">
            <a class="btn btn-outline-primary btn-padded" href="/login">Login</a>
            <a class="btn btn-outline-secondary btn-padded" href="/create-account">Sign Up</a>
          </p>
          <footer class="lead">This demo site is part of the Rodauth repository, so if you want to know how it works, you can <a href="https://gitlab.com/os85/rodauth-oauth/tree/master/examples">review the source</a>.</footer>
        <% end %>
      HTML
    end

    r.on "rotate-keys" do
      r.get do
        jws_key = OpenSSL::PKey::RSA.generate(4096)
        jws_public_key = jws_key.public_key

        PRIV_KEY.unshift(jws_key)
        PUB_KEY.unshift(jws_public_key)

        puts "rotated"
      end
    end
  end

  private

  def json_request(meth, uri, headers: {}, params: {})
    uri = URI(uri)
    http = Net::HTTP.new(uri.host, uri.port)
    case meth
    when :get
      request = Net::HTTP::Get.new(uri.request_uri)
      request["accept"] = "application/json"
      headers.each do |k, v|
        request[k] = v
      end
      response = http.request(request)
      raise "Unexpected error on token generation, #{response.body}" unless response.code.to_i == 200

      JSON.parse(response.body, symbolize_names: true)
    when :post
      request = Net::HTTP::Post.new(uri.request_uri)
      request.body = JSON.dump(params)
      request["content-type"] = "application/json"
      request["accept"] = "application/json"
      headers.each do |k, v|
        request[k] = v
      end
      response = http.request(request)
      raise "Unexpected error on token generation, #{response.body}" unless response.code.to_i == 200

      JSON.parse(response.body, symbolize_names: true)
    end
  end
end

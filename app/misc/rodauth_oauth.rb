require "openssl"

PRIV_KEY = OpenSSL::PKey::RSA.new(File.read(Rails.root.join("tmp", "rsaprivkey.pem")))
PUB_KEY = OpenSSL::PKey::RSA.new(File.read(Rails.root.join("tmp", "rsapubkey.pem")))

class RodauthOauth < RodauthMain
  configure do

    # Oauth Authorization Features
    enable :oidc,
           :oauth_pkce,
           :oauth_token_introspection,
           :oidc_dynamic_client_registration,
           :oauth_jwt_secured_authorization_request,
           # :oauth_jwt_secured_authorization_response_mode,
           :oauth_pushed_authorization_request,
           :oidc_rp_initiated_logout

    # oauth_response_mode "query.jwt"
    login_return_to_requested_location? true

    secret_matches? { |oauth_application, secret| Argon2::Password.verify_password(secret, oauth_application[oauth_applications_client_secret_hash_column]) }
    secret_hash { |secret| Argon2::Password.create(secret) }

    oauth_application_scopes %w[openid email]

    oauth_account_ds { |id| Account.where(account_id_column => id) }
    oauth_application_ds { |id| OAuthApplication.where(oauth_applications_id_column => id) }

    if Rails.env.development?
      oauth_valid_uri_schemes %w[http https]
    end

    oauth_jwt_keys("RS256" => PRIV_KEY)
    oauth_jwt_public_keys("RS256" => PUB_KEY)

    oauth_acr_values_supported { super() | %w[1 2] }
    oauth_jwt_jws_algorithms_supported { super() | %w[RS256] }

    get_oidc_param do |account, param|
      @profile ||= Profile.find_by(account_id: account[:id])
      case param
      when :email
        account[:email]
      when :email_verified
        account[:status] == "verified"
      when :name
        @profile.name
      end
    end

    check_csrf? { request.path == "/oidc-logout" ? false : super() }

    before_register do
      secret_code = ENV["HTTP_AUTHORIZATION"]
      # authorization_required unless secret_code == "12345678"
      @oauth_application_params[:account_id] = _account_from_login(ENV["ACCOUNT"])[:id]
      @oauth_application_params[:request_object_signing_alg] = "RS256"
    end
  end
end

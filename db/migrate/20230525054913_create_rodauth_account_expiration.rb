class CreateRodauthAccountExpiration < ActiveRecord::Migration[7.0]
  def change
    # Used by the account expiration feature
    create_table :account_activity_times, id: false do |t|
      t.bigint :id, primary_key: true
      t.foreign_key :accounts, column: :id
      t.datetime :last_activity_at, null: false
      t.datetime :last_login_at, null: false
      t.datetime :expired_at
    end
  end
end
